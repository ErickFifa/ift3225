<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
<xsl:template match="/">
<html>
<head>
<title>
<xsl:value-of select="recette/entete/titre"/>
</title>
</head>
<body>
<h1>
<xsl:value-of select="recette/entete/titre"/>
<hr size="6" width="75%" align="left" color="green"> </hr>
</h1>
 Auteur: <xsl:value-of select="recette/entete/auteur"/>
<br/>
Remarque:
<xsl:value-of select="recette/entete/remarque"/>
<h2>Procédure:</h2>
<h3>Liste des ingrédients :</h3>
<ul>
<xsl:for-each select="recette/procedure/liste/item">
<li>
<xsl:value-of select="."/>
</li>
</xsl:for-each>
</ul>
<h3>Les opérations :</h3>
<ul>
<xsl:for-each select="recette/procedure/texte">
<li>
<xsl:value-of select="."/>
</li>
</xsl:for-each>
</ul>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
