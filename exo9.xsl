<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:template match="/">
<html>
<body>
  <h2>mes Cds</h2>
  <table border="1">
    <tr bgcolor="green">
      <th>Titre</th>
      <th>Artiste</th>
    </tr>
    <xsl:for-each select="logitech/cd">
    <tr>
      <td><xsl:value-of select="titre"/></td>
      <xsl:choose>
      <xsl:when test="prix > 10">
         <td bgcolor="#de1738">
         <xsl:value-of select="artiste"/>
         </td>
      </xsl:when>
      <xsl:otherwise>
         <td><xsl:value-of select="artiste"/></td>
      </xsl:otherwise>
      </xsl:choose>
    </tr>
    </xsl:for-each>
  </table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
