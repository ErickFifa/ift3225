<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:template match="/recette">
<html>
<head>
<title>
<xsl:value-of select="//titre"/>

</title>
</head>
<body>
<h1>
<xsl:value-of select="//titre"/>
<hr size="6" width="75%" align="left" color="green"> </hr>
</h1>
<p>
<b>Auteur: </b>
<xsl:value-of select="//auteur"/>
</p>
<p>
<b>Remarque: </b>
<xsl:value-of select="//remarque"/>
</p>
<h2>Procédure</h2>
<p>
<xsl:value-of select="procedure"/>
</p>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
