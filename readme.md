# Déployer vos pages web sur le serveur du DIRO
-------------
## Logiciel a installer

Il y a plusieurs manieres pour transferer vos fichiers vers le serveur du DIRO. Voici quelques exemple:
  * Linux: scp, sftp, FileZilla
  * Windows: WinSCP, FileZilla
  * Macintosh: Cyberduck, FileZilla

Ce manuel utilise FileZilla parce qu'il est disponible sur toutes les plates-formes.

## FileZilla

FileZilla est un client FTP open source qui supporte SFTP.

Installation de FileZilla et connexion:

+ télécharger le client FileZilla le plus récent mais stable : http://filezilla.sourceforge.net/ en cliquant sur le lien Download FileZilla Client
+ pour les utilisateurs linux, vous pouvez utiliser le packet manager pour l'installer.
    ```bash
    sudo apt-get install filezilla
    ```
    
+ installer le logiciel.

+  Une fois le logiciel lancer, procéder à la configuration (Prenez soin de changer **<nom_d'usager_au_DIRO>** par le votre) :

  -  Hôte : arcade.iro.umontreal.ca
  -  Identifiant : <nom_d'usager_au_DIRO>
  -  Mot de passe : <votre_mot_de_passe>
  -  Port : 22
  -  cliquer sur le bouton « Connexion rapide »

Pour vous déplacer dans le répertoire qui contient vos fichiers, entrez le chemin suivant dans site distant :

    /home/www-ens/<nom_d'usager_au_DIRO>/public_html


  ![alt text](distant.png "distant")

+ Il ne vous reste plus qu'à transferer vos fichiers.

Votre site web personnel devrait se trouver à l'adresse:

    http://www-ens.iro.umontreal.ca/~<nom_d'usager_au_DIRO>/
