<?php
require 'config.php';
session_start();
//Connexion au BDD
try
{
    $conn = new PDO("mysql:host=$serveur;dbname=$bdd", $login, $motDePasse);
    echo "Connection Reussie";
    }
catch(PDOException $e)
    {
    echo "Probleme de connection: " . $e->getMessage();
    }

if($_POST['logEssaie']){   //Verifie si POST['login'] n'est pas vide
  $login = $_POST['logEssaie'];
  $motDePasse = $_POST['password'];

  //Preparation de la req SQL
  $sql = "SELECT ID,nom,prenom,login,password FROM pilote WHERE login = :login";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':login', $login);
  $stmt->execute();
  //Prends la ligne retourner par mysql
  $loginValid = $stmt->fetch(PDO::FETCH_ASSOC);

  if($loginValid == false){
      echo 'erreur dans le login';
  }else{ //login est valide
    $validPassword = !strcmp($loginValid['password'],$motDePasse) ? $motDePasse : NULL ;
  }
  if($validPassword){
     $_SESSION['login_id'] = $loginValid['ID'];
     $_SESSION['login_name'] = $loginValid['nom'];
     header('Location: home.php');
     exit;
  }else{ //mot de passe non valide
    echo "Erreur dans le mots de passe";
  }
}else{ //Si on entre dans cette partie, ca veut dire que POST['login'] est vide

}

?>
