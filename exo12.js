function factoriel(n){
 if(n != 1) {
   return n*factoriel(n-1);
 }else {
   return 1;
 }
}

// on peut reecrire factoriel plus simplement en utilisant une condition ternaire:
// return (n != 1) ? n * factoriel(n - 1) : 1;
alert(factoriel(15));
