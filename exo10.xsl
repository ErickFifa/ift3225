<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
    <h2>Mes CDs </h2>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Titre</th>
        <th>Artiste</th>
      </tr>
      <xsl:for-each select="logitech/cd">
      <xsl:sort select="artiste"/>
      <tr>
        <td><xsl:value-of select="titre"/></td>
        <td><xsl:value-of select="artiste"/></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>
