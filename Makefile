target=exercice

compile=xelatex

all:
	$(compile) $(target).tex
	$(compile) $(target).tex

new: cleanall all

clean:
	rm -rf $(target).aux  $(target).snm $(target).nav $(target).toc $(target).out $(target).bbl  $(target).blg  $(target).log $(target).nav $(target).snm $(target).toc *~ *.backup

show:
	evince $(target).pdf

cleanall: clean
	touch $(target).pdf
	rm  $(target).pdf


x: cleanall all show

e: cleanall all show
	make cleanall
