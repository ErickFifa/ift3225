<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/bibliotheque">
  <html>
      <body>
          <xsl:for-each select="film">
            <xsl:value-of select="titre" /> <br />
            <xsl:value-of select="annee" /> <br />
            <xsl:value-of select="genre" /> <br />
            <hr size="6" width="50%" align="left" color="green"> </hr>
          </xsl:for-each>

          <br />
          <br />
      </body>
  </html>
 </xsl:template>
</xsl:stylesheet>
